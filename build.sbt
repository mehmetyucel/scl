lazy val refVersion = "0.1"
lazy val refName = "com-scl-test"
lazy val outputJarName = refName + "-assembly-" + refVersion + ".jar"

crossPaths := false

test in assembly := {}

name := refName

version := refVersion

scalaVersion := "2.11.8"

resolvers += "Typesafe Repository" at "http://repo.typesafe.com/typesafe/releases/"

resolvers += "webjars" at "http://webjars.github.com/m2"

scalacOptions ++= Seq(
    "-deprecation",
    "-unchecked",
    "-feature",
    "-encoding",
    "UTF-8",
    "-Xlint"
)

parallelExecution in Test := false

libraryDependencies ++= {
    Seq(
        "org.scalatest" %% "scalatest" % "2.2.4",
        "org.mockito" % "mockito-all" % "1.9.5",
        "org.scalikejdbc" %% "scalikejdbc" % "2.4.2",
        "org.scalikejdbc" %% "scalikejdbc-test" % "2.4.2" % "test",
        "com.h2database" % "h2" % "1.4.192",
        "com.github.tototoshi" %% "scala-csv" % "1.3.1",
        "ch.qos.logback" % "logback-classic" % "1.1.7"

    )
}

libraryDependencies ~= {
    _ map {
        case m => m.exclude( "com.typesafe.scala-logging", "scala-logging_2.11" )
    }
}

mainClass := Some( "com.scl.test.TestApp" )

mainClass in assembly := Some( "com.scl.test.TestApp" )