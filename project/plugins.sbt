logLevel := Level.Warn

addSbtPlugin( "com.typesafe.sbt" % "sbt-native-packager" % "0.7.1" )

addSbtPlugin( "net.virtual-void" % "sbt-dependency-graph" % "0.7.4" )

addSbtPlugin( "com.eed3si9n" % "sbt-assembly" % "0.12.0" )

addSbtPlugin( "com.typesafe.sbt" % "sbt-s3" % "0.8" )