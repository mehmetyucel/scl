package com.scl.test

import java.io.{InputStreamReader, InputStream, BufferedReader}
import java.util.zip.GZIPInputStream

object GzipIterator {
    def apply( input: InputStream ) = {
        new GzipIterator(
            new BufferedReader(
                new InputStreamReader(
                    new GZIPInputStream( input ) ) ) )

    }
}

class GzipIterator( reader: BufferedReader ) extends Iterator[ String ] {
    override def hasNext = reader.ready( )
    override def next = reader.readLine( )

    def close() = reader.close()
}







