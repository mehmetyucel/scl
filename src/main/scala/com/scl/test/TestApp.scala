package com.scl.test

import java.sql.{Connection, DriverManager}

import com.scl.test.db.DbRepository
import com.scl.test.transform.MemberDetailsTransform
import org.h2.tools.Server

import scala.collection.JavaConversions._

object TestApp extends App {
    val server = Server.createTcpServer( ).start( )

    try {
        val gzipIterator = GzipIterator( getClass.getResourceAsStream( "/member-details.csv.gz" ) )
        val memberDetailsTransform = new MemberDetailsTransform( gzipIterator )
        val rows = memberDetailsTransform.transformAll()

        val repo = new DbRepository( )
        repo.initSchema( )
        repo.insertAll( rows )
        println( "The first ten rows ordered by vendor_id and then by last name ascending" )
        repo.firstTenRows( ).foreach( println )

        println( "A list of unique values for first name" )
        repo.uniqueFirstNames( ).foreach( println )

        println( "For each decade between 1900 and 2000" +
                " display the total number of individuals with a birth year in that decade." )
        repo.decadeGroupCount().foreach( println )


    } catch {
        case e: Exception =>
            println( e.getMessage )
    } finally {
        server.stop( )
    }

}
