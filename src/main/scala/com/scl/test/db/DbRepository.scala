package com.scl.test.db

import java.sql.Date

import scalikejdbc._

//noinspection SqlDialectInspection,SqlNoDataSourceInspection
class DbRepository( ) {
    GlobalSettings.loggingSQLAndTime = LoggingSQLAndTimeSettings(
        enabled = true,
        logLevel = 'warning
    )

    Class.forName( "org.h2.Driver" )
    ConnectionPool.singleton( "jdbc:h2:mem:test;DB_CLOSE_DELAY=-1;DATABASE_TO_UPPER=false", "", "" )
    implicit val session = AutoSession

    def initSchema( ) = {
        sql"""
           | create table member_details(
           | id serial not null primary key,
           | vendor_id NUMBER,
           | county_description VARCHAR(255),
           | county_code VARCHAR(255),
           | first_name VARCHAR(255),
           | last_name VARCHAR(255),
           | dob DATE,
           | gender VARCHAR(255)
           | )
        """.stripMargin.execute( ).apply( )

    }

    def insertAll( rows: List[ Seq[Any] ] ) = {
        sql"""
           | INSERT INTO member_details
           | ( vendor_id, county_description, county_code, first_name, last_name, dob, gender )
           | VALUES ( ?, ?, ?, ?, ?, ?, ?)
        """.stripMargin.batch( rows: _*).apply()
    }

    def firstTenRows( ): List[ MemberDetails ] = {
        sql"""
          |SELECT * from member_details order by vendor_id, last_name ASC limit 10
        """.stripMargin.map(rs => MemberDetails(rs)).list.apply()
    }

    def uniqueFirstNames() : List[String] = {
        sql"""
           |SELECT first_name from member_details group by first_name order by first_name
        """.stripMargin.map(rs => rs.string("first_name")).list.apply()
    }

    def decadeGroupCount(): List[ (Int,Int) ] = {
        sql"""
           |SELECT
           |    floor(year(dob)/10)*10 as decade,
           |    count(id) cnt
           |FROM member_details
           |GROUP BY decade
           |ORDER BY decade
        """.stripMargin.map(rs => (rs.int("decade"), rs.int("cnt")) ).list.apply()
    }
}



object MemberDetails extends SQLSyntaxSupport[MemberDetails] {
    override val tableName = "member_details"
    def apply(rs: WrappedResultSet) = new MemberDetails(
        rs.long("id"),
        rs.long("vendor_id"),
        rs.string("county_description"),
        rs.string("county_code"),
        rs.string("first_name"),
        rs.string("last_name"),
        rs.date("dob"),
        rs.string("gender")
    )
}

case class MemberDetails( id: Long,
                          vendorId : Long,
                          countyDescription: String,
                          countyCode: String,
                          firstName: String,
                          lastName:String,
                          dob:Date,
                          gender:String )



