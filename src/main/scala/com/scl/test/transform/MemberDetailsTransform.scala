package com.scl.test.transform

import java.sql.Date

import com.github.tototoshi.csv.CSVParser
import com.scl.test.GzipIterator
import com.scl.test.db.MemberDetails

class MemberDetailsTransform( iterator: Iterator[String] ) {

    def transformAll( ) = iterator
            .drop( 1 )
            .map( transformLine )
            .toList

    //def transformLine( line: String ): (Int, String, String, String, String, String) = {
    def transformLine( line: String ): Seq[ Any ] = {
        CSVParser.parse( line, '\\', ',', '"' ) match {
            case Some( List( id: String,
            countyDescription: String,
            countyCode: String,
            name: String,
            dob: String,
            gender: String ) ) =>
                val split = name.split( " " )
                Seq(
                    id.toInt,
                    countyDescription,
                    countyCode,
                    split.head,
                    split.tail.mkString( " " ),
                    dateOfBirth( dob ),
                    gender )
            // case _ =>
            // log this unexpected line format?

        }
    }

    def dateOfBirth( dob: String ) = {
        val dateString = dob match {
            case s if s.length >= 8 => s.take( 4 ) + "-" + s.slice( 4, 6 ) + "-" + s.slice( 6, 8 )
            case s if s.length == 6 => s.take( 4 ) + "-" + s.slice( 4, 6 ) + "-01"
            case s if s.length == 4 => s.take( 4 ) + "-01-01"
        }
        Date.valueOf( dateString )
    }
}



