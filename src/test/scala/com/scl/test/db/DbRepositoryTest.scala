package com.scl.test.db

import java.sql.Date

import org.scalatest.{WordSpec, Matchers}


class DbRepositoryTest extends WordSpec with Matchers {
    val dbRepository = new DbRepository( )

    dbRepository.initSchema( )
    val rows: List[ Seq[ Any ] ] =
        List(
            Seq( 1, "Slough 1", "SLG", "Nama", "Last Name", Date.valueOf( "1955-01-01" ), "F" ),
            Seq( 2, "Slough 2", "SLG", "Namb", "Last Name", Date.valueOf( "1956-01-01" ), "F" ),
            Seq( 3, "Slough 3", "SLG", "Name", "Last Name", Date.valueOf( "1965-01-01" ), "F" ),
            Seq( 4, "Slough 4", "SLG", "Name", "Last Name", Date.valueOf( "1966-01-01" ), "F" ),
            Seq( 5, "Slough 5", "SLG", "Name", "Last Name", Date.valueOf( "1967-01-01" ), "F" ),
            Seq( 6, "Slough 6", "SLG", "Name", "aast Name", Date.valueOf( "1968-01-01" ), "F" ),
            Seq( 6, "Slough 7", "SLG", "Name", "cast Name", Date.valueOf( "1970-01-01" ), "F" ),
            Seq( 6, "Slough 8", "SLG", "Name", "bast Name", Date.valueOf( "1980-01-01" ), "F" ),
            Seq( 7, "Slough 9", "SLG", "Name", "east Name", Date.valueOf( "1990-01-01" ), "F" ),
            Seq( 7, "Slough 0", "SLG", "Name", "dast Name", Date.valueOf( "1991-01-01" ), "F" ),
            Seq( 8, "Slough 1", "SLG", "Namc", "Last Name", Date.valueOf( "1952-01-01" ), "F" )
        )
    dbRepository.insertAll( rows )


    //    override def fixture( implicit session: DBSession ): Unit = {
    //    }

    "firstTenRows" should {
        "shoget first 10 rows ordered by id and last name" in {
            val expected = List(
                MemberDetails( 1, 1, "Slough 1", "SLG", "Nama", "Last Name", Date.valueOf( "1955-01-01" ), "F" ),
                MemberDetails( 2, 2, "Slough 2", "SLG", "Namb", "Last Name", Date.valueOf( "1956-01-01" ), "F" ),
                MemberDetails( 3, 3, "Slough 3", "SLG", "Name", "Last Name", Date.valueOf( "1965-01-01" ), "F" ),
                MemberDetails( 4, 4, "Slough 4", "SLG", "Name", "Last Name", Date.valueOf( "1966-01-01" ), "F" ),
                MemberDetails( 5, 5, "Slough 5", "SLG", "Name", "Last Name", Date.valueOf( "1967-01-01" ), "F" ),
                MemberDetails( 6, 6, "Slough 6", "SLG", "Name", "aast Name", Date.valueOf( "1968-01-01" ), "F" ),
                MemberDetails( 8, 6, "Slough 8", "SLG", "Name", "bast Name", Date.valueOf( "1980-01-01" ), "F" ),
                MemberDetails( 7, 6, "Slough 7", "SLG", "Name", "cast Name", Date.valueOf( "1970-01-01" ), "F" ),
                MemberDetails( 10, 7, "Slough 0", "SLG", "Name", "dast Name", Date.valueOf( "1991-01-01" ), "F" ),
                MemberDetails( 9, 7, "Slough 9", "SLG", "Name", "east Name", Date.valueOf( "1990-01-01" ), "F" )
            )
            dbRepository.firstTenRows( ) shouldBe expected
        }
    }

    "uniqueFirstNames" should {
        "get unique list of names" in {
            val expected = List(
                "Nama", "Namb", "Namc", "Name"
            )

            dbRepository.uniqueFirstNames( ) shouldBe expected
        }
    }

    "decadeGroupCount" should {
        "count people born in the same decade" in {
            val expected = List(
                (1950, 3),
                (1960, 4),
                (1970, 1),
                (1980, 1),
                (1990, 2)
            )

            dbRepository.decadeGroupCount() shouldBe expected
        }
    }


}



