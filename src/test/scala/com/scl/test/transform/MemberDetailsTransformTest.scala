package com.scl.test.transform

import java.sql.Date

import org.scalatest.mock.MockitoSugar
import org.scalatest.{Matchers, BeforeAndAfter, WordSpec}
import org.mockito.Mockito._

class MemberDetailsTransformTest extends WordSpec
                                         with BeforeAndAfter
                                         with MockitoSugar
                                         with Matchers {

    var memberDetailsTransform : MemberDetailsTransform = _

    "dateOfBirth" should {
        memberDetailsTransform = new MemberDetailsTransform( mock[Iterator[String]])
        "handle 8 numbers" in {
            memberDetailsTransform.dateOfBirth("19821021") shouldBe Date.valueOf("1982-10-21")
        }
        "handle 6 numbers" in {
            memberDetailsTransform.dateOfBirth("198210") shouldBe Date.valueOf("1982-10-01")
        }
        "handle 4 numbers" in {
            memberDetailsTransform.dateOfBirth("1982") shouldBe Date.valueOf("1982-01-01")
        }
    }

    "transformAll" should {
        val testData = Iterator(
            "header",
            "5,County,Code,Name LastName,19821021,dodo",
            "6,County,Code,Name Mid Last,19821021,dodo",
            """7,"County, comma, county",Code,Name LastName,19821021,dodo"""
        )
        memberDetailsTransform = new MemberDetailsTransform(testData)
        "create a list of sequences" in {
            val expected: List[ Seq[ Any ] ] = List(
                Seq( 5, "County","Code","Name", "LastName",Date.valueOf("1982-10-21"),"dodo"),
                Seq( 6, "County","Code","Name", "Mid Last",Date.valueOf("1982-10-21"),"dodo"),
                Seq( 7, "County, comma, county","Code","Name", "LastName",Date.valueOf("1982-10-21"),"dodo")
            )
            memberDetailsTransform.transformAll() shouldBe     expected
        }
    }

}



